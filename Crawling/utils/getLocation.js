"use strict";

var nodeGeocoder = require("node-geocoder");
var options = require("../configurations/geocoderConfig");

var geocoder = nodeGeocoder(options);

function getLocation(tweet, location) {
    return new Promise(function(resolve, reject) {
        try {
            geocoder
                .geocode(location)
                .then(function(res) {
                    if (typeof res != "undefined" && res.length != 0) {
                        resolve({
                            country: res[0].country,
                            city: res[0].city,
                            latitude: res[0].latitude,
                            longitude: res[0].longitude,
                            tweet: tweet
                        });
                    } else {
                        reject("No location found");
                    }
                })
                .catch(function(err) {
                    console.log(err);
                });
        } catch (e) {
            console.log("Invalid location name");
        }
    });
}

module.exports = getLocation;
