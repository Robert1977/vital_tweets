"use strict";

var Twitter = require("twitter-node-client").Twitter;
var config = require("../configurations/twitterconfig");
var readData = require("../database/databaseFunctions").readData;
var insertTweetIntoDatabase = require("../database/databaseFunctions")
    .insertTweetIntoDatabase;
var calendarHelpers = require("../utils/calendarHelpers");
var { filter } = require("../utils/stringHelpers");
var getLocation = require("../utils/getLocation");

var twitter = new Twitter(config);
var tweets = {},
    disease = "";

function handleError(err, response, body) {
    console.log(JSON.stringify(err));
}

function success(data) {
    tweets = JSON.parse(data).statuses;
    disease = JSON.parse(data).search_metadata.query;

    for (var index in Object.keys(tweets)) {
        var location = tweets[index].user.location;
        var created = tweets[index].created_at;
        var username = tweets[index].user.name;
        var text = tweets[index].text;

        if (location && created && disease && username && text) {
            getLocation(tweets[index], location)
                .then(function(object) {
                    insertTweetIntoDatabase({
                        id: object.tweet.id_str,
                        created_at: calendarHelpers.convertDate(
                            object.tweet.created_at
                        ),
                        longitude: object.longitude,
                        latitude: object.latitude,
                        id_boala: disease,
                        username: object.tweet.user.name,
                        text: filter(object.tweet.text),
                        profilePicture:
                            object.tweet.user.profile_image_url_https,
                        language: object.tweet.lang,
                        shares: object.tweet.retweet_count,
                        likes: object.tweet.favorite_count,
                        screen_name: object.tweet.user.screen_name,
                        userId: object.tweet.user.id_str,
                        country: object.country,
                        city: object.city
                    });
                })
                .catch(function(err) {
                    console.log(err);
                });
        }
    }
}

function action() {
    readData()
        .then(function(object) {
            Object.keys(object).forEach(element => {
                for (var j = 0; j < object[element].length; j++) {
                    twitter.getCustomApiCall(
                        "/search/tweets.json",
                        {
                            q: object[element][j],
                            count: "200",
                            lang: element,
                            result_type: "recent"
                        },
                        handleError,
                        success
                    );
                }
            });
        })
        .catch(function(err) {
            console.log(err);
        });
}

function crawlingAction() {
    // Se va executa functia 'action' la un interval de 3 ore de la pornirea server-ului
    setInterval(action, 120 * 60000);
}

module.exports = {
    crawlingAction: crawlingAction,
    action: action
};
