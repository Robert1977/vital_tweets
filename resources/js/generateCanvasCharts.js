const tweetsUrl = '../resources/json/tweets.json';

fetch(tweetsUrl)
     .then(response => response.text())
     .then(promise => getLocalStorageItems(JSON.parse(promise)));

function getLocalStorageItems(json) {
    var disease = localStorage.getItem('disease');
    var country = localStorage.getItem('regions');

    if(null != disease && null != country) {
        var button = document.getElementById('create-stat');
        button.addEventListener('click', () => {
            var disease = localStorage.getItem('disease');
            var country = localStorage.getItem('regions');

            generateChart(json, disease, country);
        });
    }
    else
    {
        console.log("Invalid parameters!");
    }
}

function generateChart(json, disease, country) {
    var deleteCanvas = document.getElementById("canvas-id");
    if(null != deleteCanvas) {
        deleteCanvas.remove();
    }

    var canvas = document.createElement('canvas');
    canvas.id = "canvas-id";
    canvas.width = window.innerWidth * 0.7;
    canvas.height = window.innerHeight * 0.6;

    var countDisease = 0;
    json.forEach(jsonObject => {
        if(disease == jsonObject.disease) {
            ++countDisease;
        }
    });

    new Chart(canvas, {
        type: 'bar',
        data: {
            labels: [country],
            datasets: [
            {
                label: "Population (millions)",
                backgroundColor: ["#8e5ea2", "#3e95cd","#3cba9f","#e8c3b9","#c45850"],
                data: [countDisease, 0, 200]
            }
        ]
        },
        options: {
            legend: { display: true },
            title: {
                display: true,
                text: disease
            },
            responsive: false,
            scales: {
                xAxes: [{
                    barPercentage: 0.4
                }]
            }
        }
    });

    var main = document.getElementById("main-id");
    main.appendChild(canvas);
}
