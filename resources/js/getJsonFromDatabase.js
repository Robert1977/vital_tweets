function getJsonInterval(){

var http = require('http');
var mysql = require('mysql');
var fs = require('fs');

fs.readFile('../../DB/credentials.txt', "utf8", function (err, strCredentials) {
	var credentials =
		strCredentials.split("\r\n");

	var con = mysql.createConnection({
		host: credentials[0],
		user: credentials[1],
		password: credentials[2],
		database: credentials[3],
		ssl: true
	});

	var connectionSuccessful = -1;
	var jsonResult = -1;
	con.connect(function (err) {
		if (err) throw err;

		connectionSuccessful = 0;
		console.log("Connected!");

		var sql = "SELECT * FROM Tweets;";

		con.query(sql, function (err, result) {
			if (err) throw err;

			fs.writeFile('../json/Tweets.json', JSON.stringify(result), (err) => {
				if (err) throw err;
			});
		});

		sql = "SELECT * FROM keyTerms;";

		con.query(sql, function (err, result) {
			if (err) throw err;

			fs.writeFile('../json/keyTerms2.json', JSON.stringify(result), (err) => {
				console.log(err);

				if (null != result) {
					jsonResult = 0;
				}
			});
		});

		console.log("Disconnected!");

		con.end();
	});
});
}

setInterval(getJsonInterval, 3 600 000);