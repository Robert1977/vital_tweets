### Pentru a urca un numar cat mai mic de fisiere si pentru a fi cat mai eficienti ne vom folosi de fisierul .gitignore si nodeJs.

1. Un prim pas ar fi instalarea locala a NodeJs-usului https://nodejs.org/en/download/ ( ne ajuta sa luam pachete de pe https://www.npmjs.com )
2. Pentru instalarea tuturor pachetelor prezente in proiect dati in terminal comanda 'npm i'
2. Instalarea unui pachet se face urmarind instructiunile de pe npmjs din pagina modulului respectiv

### Making Changes


Please follow these steps when creating/adding something new to the repository.

#### Starting Work on a new Issue

1. Before uploading any code make sure you have the last version of master:
    - git checkout master  //// moves you to branch 'master'
    - git pull             //// gets the last version of master

2. Create a new branch for your changes. Try to use the standard naming convention `issue-000` to match changes to the related redmine issue.
    - git checkout -b issue-000  // -b => create branch
                                 // issue-000 => name of the branch

3. Synchronise this local branch back to BitBucket.
    - git push --set-upstream origin issue-000


#### Restarting Work on a Previously-submitted Issue

1. Make sure you have no outstanding changes.
    - (from the project directory)
    - `> git status`
    - The resulting output should not list any files that are Changed or Untracked. If you have changes then please commit them, stash them or roll them back. Please contact the programmers or consult Git documentation for more info.
2. Get the latest copy of your previous branch and push it back to the server.
    - `> git checkout issue/redmine-0000`
3. (optional) Merge (and conflict-resolve) any changes that have since been made to the master branch
    - `> git fetch origin master`
    - `> git merge --no-ff origin/master`
4. Re-open this local branch on BitBucket and synchronise any changes.
    - `> git push --set-upstream origin issue-000`


#### Submitting code changes

##1. Make and test small, self-contained changes.
2. Use git to keep track what changes you have made
    - `> git status` Will list the files you have yet to commit along with details around your sync status to BitBucket
    - `> git diff <file> [...<file>]` Will show you each added/removed/changed line in a file since your last commit
3. Once you are happy with each change, commit it to your local branch.
    - `> git add <file>` or `> git add -A` Will add the changes you have made to a 'stage' that can then be committed.
    - `> git status` To ensure all the files you want to commit are listed in the 'stage' area.
    - `> git commit -m "Description of my change"` Will commit all staged changes ready to sync to BitBucket.
4. Before you leave your desk, sync the branch back to BitBucket again.
    - `> git push` Other contributors will now be able to see changes on BitBucket.